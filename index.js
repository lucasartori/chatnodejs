var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var redis = require('redis');
var redisClient = redis.createClient();

app.use(express.static('public'));
app.use(express.static('files'));
app.use(express.static('bower_components'));
app.use('/assets', express.static(__dirname + '/assets'));

var people = {};

var storeMessages = function (name, data){
  var message = JSON.stringify({name: name, data: data});
  redisClient.lpush("messages", message, function(err, response){
    redisClient.ltrim("messages", 0, 9);
  });  
};
app.get('/', function(req, res) {
  res.sendFile( __dirname + '/index.html');
});


io.on('connection', function(client) {
  client.on('join', function(name) {
    people[client.id]= name;
    client.broadcast.emit("update", people[client.id] + " has join the server.");
    
    io.sockets.emit("update-people", people);

    redisClient.lrange("messages", 0,-1, function(err, messages){
      messages = messages.reverse();
      messages.forEach(function(msg){
        msg = JSON.parse(msg);
        client.emit('chat message',msg.name +": " + msg.data);
      });
    });
  });
  
  client.on('chat message', function(msg) {
    io.sockets.emit('chat message',people[client.id] +": " + msg);
    storeMessages(people[client.id], msg);
  });

  client.on('disconnect', function() {
    client.broadcast.emit("update", people[client.id] + " has left the server.");
    delete people[client.id];
    client.broadcast.emit("update-people", people);
  });
});


http.listen(3000, function() {
  console.log('listening on *:3000');
});